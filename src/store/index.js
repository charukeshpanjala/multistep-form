import { configureStore } from "@reduxjs/toolkit"

const stepReducer = (state = { selectedStep: 1 }, action) => {
    switch (action.type) {
        case "next":
            if (state.selectedStep < 4) {
                return { selectedStep: state.selectedStep + 1 }
            } else {
                return { selectedStep: state.selectedStep }
            }
        case "prev":
            if (state.selectedStep > 1) {
                return { selectedStep: state.selectedStep - 1 }
            } else {
                return { selectedStep: state.selectedStep }
            }
        default:
            return state
    }
}

const step1FormReducer = (state = { firstName: "", lastName: "", phoneNumber: "", email: "" }, action) => {
    switch (action.type) {
        case "firstName":
            return { ...state, firstName: action.value }
        case "lastName":
            return { ...state, lastName: action.value }
        case "phoneNumber":
            return { ...state, phoneNumber: action.value }
        case "email":
            return { ...state, email: action.value }
        default:
            return state
    }
}


const step1FormValidator = (state = { firstNameError: "", lastNameError: "", phoneNumberError: "", emailError: "" }, action) => {
    switch (action.type) {
        case "submitStep1Form":
            let updatedState = { ...state }
            updatedState.firstNameError = (action.value.firstName.length === 0) ? "Enter Your First Name" : ""
            updatedState.lastNameError = (action.value.lastName.length === 0) ? "Enter Your Last Name" : ""
            updatedState.phoneNumberError = (action.value.phoneNumber.length === 0) ? "Enter Your Phone Number" : (action.value.phoneNumber.length !== 10 ? "Enter Valid Phone Number" : "")
            updatedState.emailError = (action.value.email.length === 0) ? "Enter Your Email Address" : ((action.value.email.includes("@") && action.value.email.includes(".com")) ? "" : "Enter Valid Email Address")
            return updatedState;
        default:
            return state
    }
}

const step2FormValidator = (state = { count: 1, schools: { school1: "" } }, action) => {
    switch (action.type) {
        case "addSchool":
            const newSchool = "school" + (state.count + 1)
            return { count: state.count + 1, schools: { ...state.schools, [newSchool]: "" } }
        case "updateSchool":
            let updatedState = { ...state, schools: { ...state.schools } }
            updatedState.schools[action.id] = action.value
            return updatedState
        default:
            return state
    }
}

const step3FormValidator = (state = { 1: ["", ""] }, action) => {
    switch (action.type) {
        case "addExperience":
            const newExperience = (Object.keys(state).length + 1)
            return { ...state, [newExperience]: ["", ""] }
        case "updateExperience":
            let updatedState = { ...state }
            updatedState[action.id][0] = action.value
            return updatedState
        case "updatePosition":
            let updatedPostion = { ...state }
            updatedPostion[action.id][1] = action.value
            return updatedPostion
        default:
            return state
    }
}

const store = configureStore({ reducer: { stepReducer, step1FormReducer, step1FormValidator, step2FormValidator, step3FormValidator } })

export default store