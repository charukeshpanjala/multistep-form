import "./index.css";

function StepHeading(props) {
    return (<>
        <div className="emoji-container">
            <img src={props.emoji} alt="step1 emoji" className="emoji" />
        </div>
        <h1 className="form-heading">
            {props.heading}
        </h1>
        <p className="form-description">
            {props.description}
        </p>
    </>)
}

export default StepHeading;