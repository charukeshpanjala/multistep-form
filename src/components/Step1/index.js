import Form from "../Form/index";
import StepHeading from "../StepHeading/index";
import step1Emoji from "../../Logos/step1emoji.png"
import { useDispatch, useSelector } from "react-redux";

import "./index.css";

function Step1() {
    const step1Details = useSelector(state => state.step1FormReducer)
    const step1FormErrors = useSelector(state => state.step1FormValidator)
    const dispatch = useDispatch()

    function onChangeFirstName(event) {
        dispatch({ type: "firstName", value: event.target.value })
    }

    function onChangeLastName(event) {
        dispatch({ type: "lastName", value: event.target.value })
    }

    function onChangePhoneNumber(event) {
        dispatch({ type: "phoneNumber", value: event.target.value })
    }

    function onChangeEmail(event) {
        dispatch({ type: "email", value: event.target.value })
    }

    function onClickBack() {
        dispatch({ type: "prev" })
    }

    function onClickSubmit(event) {
        event.preventDefault()
        const { firstName, lastName, email, phoneNumber } = step1Details
        const { firstNameError, lastNameError, emailError, phoneNumberError } = step1FormErrors
        dispatch({ type: "submitStep1Form", value: step1Details })
        if (firstName.length > 0 && firstNameError.length === 0 && lastName.length > 0 && lastNameError.length === 0 && email.length > 0 && emailError.length === 0 && phoneNumber.length === 10 && phoneNumberError.length === 0) {
            dispatch({ type: "next" })
        }
    }

    const step1Form = (<div className="step1-form">
        <StepHeading emoji={step1Emoji} heading="Your Personal Information" description="Enter your personal information to get closer to companies." />
        <form className="step1-form-container" onSubmit={onClickSubmit}>
            <div className="label-input-container">
                <label htmlFor="firstName" className="label">First Name</label>
                <input type="text" className="text-input" id="firstName" placeholder="First Name" value={step1Details.firstName} onChange={onChangeFirstName} />
                {step1FormErrors.firstNameError !== "" && <p className="err">*{step1FormErrors.firstNameError}</p>}
            </div>
            <div className="label-input-container">
                <label htmlFor="lastName" className="label">Last Name</label>
                <input type="text" className="text-input" id="lastName" placeholder="Last Name" value={step1Details.lastName} onChange={onChangeLastName} />
                {step1FormErrors.lastNameError !== "" && <p className="err">*{step1FormErrors.lastNameError}</p>}

            </div>
            <div className="label-input-container">
                <label htmlFor="phoneNumber" className="label">Phone Number</label>
                <input type="number" className="text-input" id="phoneNumber" placeholder="Phone Number" value={step1Details.phoneNumber} onChange={onChangePhoneNumber} />
                {step1FormErrors.phoneNumberError !== "" && <p className="err">*{step1FormErrors.phoneNumberError}</p>}

            </div>
            <div className="label-input-container">
                <label htmlFor="emailAddress" className="label">E-mail Adress</label>
                <input type="text" className="text-input" id="emailAddress" placeholder="E-mail" value={step1Details.email} onChange={onChangeEmail} />
                {step1FormErrors.emailError !== "" && <p className="err">*{step1FormErrors.emailError}</p>}

            </div>
            <div>
                <button className="back-btn" type="button" onClick={onClickBack}>Back</button>
                <button className="next-btn" type="submit">Next Step</button>
            </div>
        </form>
    </div>)
    return (<Form form={step1Form} step="Step 1" stepDescription="Enter your personal information to get closer to companies." />);
}

export default Step1;