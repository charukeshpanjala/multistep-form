import Form from "../Form";
import StepHeading from "../StepHeading";
import step4Emoji from "../../Logos/step4.emoji.png"
import upload from "../../Logos/upload.png";
import { useDispatch } from "react-redux"
import { useRef, useState } from "react"
import "./index.css"

function Step4() {
    const dispatch = useDispatch()
    const inputFile = useRef(null)

    const [profile, updateProfile] = useState(upload)

    function onClickBack() {
        dispatch({ type: "prev" })
    }

    function onClickNext() {
        dispatch({ type: "next" })
    }

    const onButtonClick = () => {
        inputFile.current.click();
    };

    function onChangeFile(event) {
        event.stopPropagation();
        event.preventDefault();
        // var file = event.target.files[0];
        // updateProfile(file);
        const [file] = (event.target.files)
        updateProfile(URL.createObjectURL(file));
    }

    const step4 = (<div className="step1-form">
        <StepHeading heading="User Photo" description="Upload your profile picture and show yourself." emoji={step4Emoji} />
        <div className="image-container">
            <div className="upload-image-container" onClick={onButtonClick}>
                <input type='file' id='file' ref={inputFile} style={{ display: 'none' }} onChange={onChangeFile} />
                <img src={profile} alt="Profile" className="upload-image" />
            </div>
        </div>
        <div>
            <button className="back-btn" type="button" onClick={onClickBack}>Back</button>
            <button className="next-btn" type="submit" onClick={onClickNext}>Next Step</button>
        </div>
    </div >)
    return (<Form form={step4} step="Step 4" stepDescription="Add your profile picture and let companies find you fast." />);
}

export default Step4;