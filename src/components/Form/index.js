import logo from "../../Logos/indeed.svg";
import "./index.css"
import { useSelector } from "react-redux";

function Form(props) {
    const val = useSelector(store => store.stepReducer.selectedStep)
    let step1Class = "step"
    let step1Span = "step-count"
    let step2Class = "step"
    let step2Span = "step-count"
    let step3Class = "step"
    let step3Span = "step-count"
    let step4Class = "step"
    let step4Span = "step-count"
    switch (val) {
        case 1:
            step1Class = "step selected-step";
            step1Span = "step-count selected-step";
            break
        case 2:
            step2Class = "step selected-step";
            step2Span = "step-count selected-step";
            break;
        case 3:
            step3Class = "step selected-step";
            step3Span = "step-count selected-step";
            break;
        case 4:
            step4Class = "step selected-step";
            step4Span = "step-count selected-step";
            break;
        default:
            break;
    }

    return (<div className="form-container">
        <div className="steps-logo-container">
            <img src={logo} className="indeed-logo" alt="indeed-logo" />
            <div className="steps-container">
                <h1 className="step-name">{props.step}</h1>
                <p className="step-description">{props.stepDescription}</p>
                <p className={step1Class}><span className={step1Span}>1</span> Personal Information</p>
                <div className="dotted-line"></div>
                <p className={step2Class}><span className={step2Span}>2</span> Education</p>
                <div className="dotted-line"></div>
                <p className={step3Class}><span className={step3Span}>3</span> Work Experiences</p>
                <div className="dotted-line"></div>
                <p className={step4Class}><span className={step4Span}>4</span> User Photo</p>
            </div>
        </div>
        {props.form}
    </div>);
}

export default Form;