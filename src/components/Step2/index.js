import Form from "../Form/index";
import step2Emoji from "../../Logos/step2emoji.png"
import StepHeading from "../StepHeading";
import { useDispatch, useSelector } from "react-redux";
import "./index.css";

function Step2() {
    const dispatch = useDispatch()
    const schools = useSelector(state => state.step2FormValidator.schools)
    function onClickBack() {
        dispatch({ type: "prev" })
    }

    function onClickAddSchool() {
        dispatch({ type: "addSchool" })
    }

    function onClickNext(event) {
        event.preventDefault()
        dispatch({ type: "next" })
    }

    const step1Form = (<div className="step1-form">
        <StepHeading heading="Education" description="Inform companies  about your education  life." emoji={step2Emoji} />
        <form className="step2-form-container" onSubmit={onClickNext}>
            {Object.keys(schools).map(eachSchool => {
                return (<div className="school-label-input-container" key={eachSchool}>
                    <label className="label">School</label>
                    <input type="search" className="text-input" placeholder="Add School Name" value={schools[eachSchool]} onChange={(event) => dispatch({ type: "updateSchool", value: event.target.value, id: eachSchool })} />
                </div>)
            })}
            <div className="school-label-input-container">
                <label htmlFor="school1" className="label">School</label>
                <button className="add-school-button" type="button" onClick={onClickAddSchool}>Add New School</button>
            </div>

            <div>
                <button className="back-btn" type="button" onClick={onClickBack}>Back</button>
                <button className="next-btn" type="submit">Next Step</button>
            </div>
        </form>
    </div>)
    return (<Form form={step1Form} step="Step 2" stepDescription="Get to know better by adding your diploma, certificate and education life." />);
}

export default Step2;