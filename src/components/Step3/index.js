import Form from "../Form/index";
import step3Emoji from "../../Logos/step3emoji.png"
import StepHeading from "../StepHeading";
import { useDispatch, useSelector } from "react-redux";
import "./index.css";
import React from "react";

function Step3() {
    const dispatch = useDispatch()
    const experience = useSelector(state => state.step3FormValidator)
    function onClickBack() {
        dispatch({ type: "prev" })
    }


    function onClickNext(event) {
        event.preventDefault()
        dispatch({ type: "next" })
    }

    function onClickAddExperience(){
        dispatch({ type: "addExperience"})
    }

    const step1Form = (<div className="step1-form">
        <StepHeading heading="Work Experiences" description="Can you talk about your past work experience?" emoji={step3Emoji} />
        <form className="step2-form-container" onSubmit={onClickNext}>
            <div className="step1-form-container">
                {Object.keys(experience).map(eachItem => {
                    return (
                        <React.Fragment key={eachItem}>
                            <div className="label-input-container">
                                <label htmlFor="firstName" className="label">Experience</label>
                                <input type="text" className="text-input" id="firstName" placeholder="Experience" value={experience[eachItem][0]} onChange={(event) => dispatch({ type: "updateExperience", id: eachItem, vaue: event.target.value })} />
                            </div>
                            <div className="label-input-container">
                                <label htmlFor="position" className="label">Position</label>
                                <input type="text" className="text-input" id="firstName" placeholder="Position" value={experience[eachItem][1]} onChange={(event) => dispatch({ type: "updatePosition", id: eachItem, vaue: event.target.value })} />
                            </div>
                        </React.Fragment>
                    )
                })}
            </div>
            <div className="school-label-input-container block">
                <label htmlFor="school1" className="label">Add Experience</label>
                <button className="add-school-button" type="button" onClick={onClickAddExperience}>Add New Experience</button>
            </div>
            <div>
                <button className="back-btn" type="button" onClick={onClickBack}>Back</button>
                <button className="next-btn" type="submit">Next Step</button>
            </div>
        </form >
    </div >)
    return (<Form form={step1Form} step="Step 3" stepDescription="Help companies get to know you better by telling them about your past experiences." />);
}

export default Step3;