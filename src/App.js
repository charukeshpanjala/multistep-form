import Step1 from "./components/Step1"
import Step2 from "./components/Step2"
import Step3 from "./components/Step3"
import Step4 from "./components/Step4"
import { useSelector } from "react-redux";
import "./App.css"

function App() {
  const selectedPage = useSelector(state => state.stepReducer.selectedStep)
  switch (selectedPage) {
    case 1:
      return <Step1 />
    case 2:
      return <Step2 />
    case 3:
      return <Step3 />
    case 4:
      return <Step4 />
    default:
      return <Step1 />
  }
}

export default App;
